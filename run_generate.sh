#!/bin/bash

#UFOlocation=$1

export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

#asetup 19.2.5.16.1,MCProd,here # does not work (https://bugs.launchpad.net/mg5amcnlo/+bug/1657615)
asetup 19.2.5.14.5,MCProd,here
#export PYTHONPATH=${UFOlocation}:$PYTHONPATH
export MAKEFLAGS="-j1 QUICK=1 -l8"
#export ATHENA_PROC_NUMBER=8
#Generate_tf.py --ecmEnergy=13000 --runNumber=999999 --maxEvents=1 --jobConfig=MC15.999999.MGPy8EG_A14N23LO_Pseudoscalar_2HDM_test.py --outputEVNTFile=blabla.root
Generate_tf.py --ecmEnergy=13000 --runNumber=999999 --maxEvents=1000 --jobConfig=MC15.999999.MGPy8EG_A14N23LO_Pseudoscalar_2HDM_test.py --outputEVNTFile=blabla.root
