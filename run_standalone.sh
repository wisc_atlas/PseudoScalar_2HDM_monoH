#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup root # will set up gcc 4.9


wget https://launchpad.net/mg5amcnlo/2.0/2.4.x/+download/MG5_aMC_v2.4.3.tar.gz
tar -zxvf MG5_aMC_v2.4.3.tar.gz
cd MG5_aMC_v2_4_3
svn co svn+ssh://svn.cern.ch/reps/atlasphys-exo/Physics/Exotic/JDM/DMsummary/SummaryPaper/PseudoScalar_2HDM/Pseudoscalar_2HDM models/Pseudoscalar_2HDM
mkdir -p run

cd run

echo "******************************************"
echo "*     what you need to do:               *"
echo "*     1.  ../bin/mg5_aMC                  *"
echo "*     2.  paste the following            *"
echo "******************************************"
echo "set automatic_html_opening false"
echo "import model Pseudoscalar_2HDM -modelname"
echo "define p = g d u s c d~ u~ s~ c~"
echo "define j = g d u s c d~ u~ s~ c~ b b~ t t~"
echo "define dm = Xd Xd~"
echo "generate p p > dm dm h1 [QCD]"
echo "output -f"
echo "launch"
